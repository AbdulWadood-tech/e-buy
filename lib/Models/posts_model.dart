import 'dart:collection';

class PostModel {
  String photoUrl;
  String price;
  String title;
  bool isMe;
  String description;
  String city;
  String email;
  String phoneNo;
  String uid;
  PostModel({
    required this.photoUrl,
    required this.uid,
    required this.email,
    required this.phoneNo,
    required this.isMe,
    required this.price,
    required this.city,
    required this.description,
    required this.title,
  });
}
