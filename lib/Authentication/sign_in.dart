import 'package:e_buy/Authentication/Services/services.dart';
import 'package:e_buy/Databas%20Services/database_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SignIn extends StatefulWidget {
  final dynamic function;
  SignIn({Key? key, required this.function}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  String? email;
  String? password;
  bool loading = false;
  getLoader() {
    setState(() {
      loading = !loading;
    });
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? Scaffold(
            body: Center(
              child: SpinKitCircle(
                color: Colors.grey,
                size: 50,
              ),
            ),
          )
        : Scaffold(
            body: SingleChildScrollView(
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                child: Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.2),
                  child: Form(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Neumorphic(
                              style: NeumorphicStyle(
                                  shape: NeumorphicShape.convex,
                                  boxShape: NeumorphicBoxShape.roundRect(
                                      BorderRadius.circular(20)),
                                  depth: -5,
                                  lightSource: LightSource.topLeft,
                                  color: Colors.white),
                              child: Container(
                                padding: EdgeInsets.only(top: 5, left: 5),
                                height: 60,
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: TextFormField(
                                  onChanged: (val) {
                                    email = val;
                                  },
                                  decoration: InputDecoration(
                                    hintText: 'Email',
                                    hintStyle: TextStyle(fontSize: 14),
                                    border: InputBorder.none,
                                    prefixIcon: Icon(
                                      Icons.email,
                                      color: Colors.black38,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Neumorphic(
                              style: NeumorphicStyle(
                                  shape: NeumorphicShape.convex,
                                  boxShape: NeumorphicBoxShape.roundRect(
                                      BorderRadius.circular(20)),
                                  depth: -5,
                                  lightSource: LightSource.topLeft,
                                  color: Colors.white),
                              child: Container(
                                padding: EdgeInsets.only(top: 5, left: 5),
                                height: 60,
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: TextFormField(
                                  onChanged: (val) {
                                    password = val;
                                  },
                                  decoration: InputDecoration(
                                    hintText: 'Password',
                                    hintStyle: TextStyle(fontSize: 14),
                                    border: InputBorder.none,
                                    prefixIcon: Icon(
                                      Icons.password_outlined,
                                      color: Colors.black38,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            TextButton(
                              onPressed: () async {
                                getLoader();
                                try {
                                  var user = await AuthenticationServices()
                                      .signInUserWithEmailAndPassword(
                                          email!, password!);
                                } catch (e) {
                                  print(e);
                                  getLoader();
                                }
                              },
                              child: Text('Sign in'),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width * 0.1,
                                    child: Divider(
                                      thickness: 0.5,
                                      color: Colors.black38,
                                    )),
                                SizedBox(width: 10),
                                Text(
                                  'OR',
                                  style: TextStyle(color: Colors.grey),
                                ),
                                SizedBox(width: 10),
                                SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width * 0.1,
                                    child: Divider(
                                      thickness: 0.5,
                                      color: Colors.black38,
                                    )),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Neumorphic(
                                    style: NeumorphicStyle(
                                        shape: NeumorphicShape.convex,
                                        boxShape: NeumorphicBoxShape.roundRect(
                                            BorderRadius.circular(40)),
                                        depth: 5,
                                        lightSource: LightSource.topLeft,
                                        color: Colors.white),
                                    child: Container(
                                      height: 60,
                                      width: 60,
                                      child: IconButton(
                                        icon: Icon(
                                          FontAwesome.google,
                                          color: Colors.red[300],
                                          size: 30,
                                        ),
                                        onPressed: () async {
                                          getLoader();
                                          try {
                                            var user =
                                                await AuthenticationServices()
                                                    .signInWithGoogle();
                                            DatabaseServices()
                                                .addUserToDatabase(
                                                    user.uid,
                                                    user.displayName,
                                                    user.photoURL,
                                                    user.email);
                                          } catch (e) {
                                            print(e);
                                            getLoader();
                                          }
                                        },
                                      ),
                                    )),
                                SizedBox(
                                  width: 20,
                                ),
                                Neumorphic(
                                    style: NeumorphicStyle(
                                        shape: NeumorphicShape.convex,
                                        boxShape: NeumorphicBoxShape.roundRect(
                                            BorderRadius.circular(40)),
                                        depth: 5,
                                        lightSource: LightSource.topLeft,
                                        color: Colors.white),
                                    child: Container(
                                      height: 60,
                                      width: 60,
                                      child: IconButton(
                                          icon: Icon(
                                            FontAwesome.facebook,
                                            color: Colors.blue[500],
                                            size: 30,
                                          ),
                                          onPressed: () async {
                                            getLoader();
                                            try {
                                              var user =
                                                  await AuthenticationServices()
                                                      .signInWithFacebook();
                                              DatabaseServices()
                                                  .addUserToDatabase(
                                                user.uid,
                                                user.displayName,
                                                user.photoURL,
                                                user.email,
                                              );
                                            } catch (e) {
                                              print(e);
                                              getLoader();
                                            }
                                          }),
                                    )),
                              ],
                            ),
                          ],
                        ),
                        Spacer(),
                        TextButton(
                          onPressed: this.widget.function,
                          child: Text('Don\'t have an account'),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
  }
}
