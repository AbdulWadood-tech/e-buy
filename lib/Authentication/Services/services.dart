import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthenticationServices {
  FirebaseAuth _auth = FirebaseAuth.instance;
  GoogleSignIn _googleSignIn = GoogleSignIn();
  FacebookAuth _fAuth = FacebookAuth.instance;

  //create user with email and password
  Future createUserWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth
          .createUserWithEmailAndPassword(email: email, password: password);
      User? user = userCredential.user;
      return user;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //Sign in user with existing email and password
  Future signInUserWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User? user = userCredential.user;
      return user;
    } catch (e) {
      print(e);
      return null;
    }
  }

  //Stream to get user from auth if exist
  Stream<User?> get getUserStream {
    return _auth.authStateChanges();
  }

  //Log out existing user
  Future<void> signOutUser() async {
    try {
      _auth.signOut();
      _googleSignIn.disconnect();
    } catch (e) {
      print(e);
    }
  }

  //sign in using google
  Future signInWithGoogle() async {
    try {
      final user = await _googleSignIn.signIn();
      if (user != null) {
        final googleAuth = await user.authentication;
        final credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        final result = await _auth.signInWithCredential(credential);
        User? authUser = result.user;
        return authUser;
      }
    } catch (e) {
      print(e.toString());
    }
  }

  //sign in using facebook
  Future signInWithFacebook() async {
    try {
      final result =
          await _fAuth.login(permissions: ['email', 'public_profile']);
      switch (result.status) {
        case LoginStatus.success:
          print('Succeed');
          final AuthCredential facebookCredential =
              FacebookAuthProvider.credential(result.accessToken!.token);
          final userCredential =
              await _auth.signInWithCredential(facebookCredential);
          User? user = userCredential.user;
          return user;
      }
    } catch (e) {
      print(e.toString());
    }
  }

  //get Email
  String? getEmail() {
    return _auth.currentUser!.email;
  }
}
