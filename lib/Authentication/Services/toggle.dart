import 'package:e_buy/Authentication/sign_in.dart';
import 'package:e_buy/Authentication/sign_up.dart';
import 'package:flutter/cupertino.dart';

class Toggle extends StatefulWidget {
  const Toggle({Key? key}) : super(key: key);

  @override
  _ToggleState createState() => _ToggleState();
}

class _ToggleState extends State<Toggle> {
  bool toggle = true;
  isToggle() {
    setState(() {
      toggle = !toggle;
    });
  }

  @override
  Widget build(BuildContext context) {
    return toggle
        ? SignIn(function: () {
            isToggle();
          })
        : SignUp(
            function: () {
              isToggle();
            },
          );
  }
}
