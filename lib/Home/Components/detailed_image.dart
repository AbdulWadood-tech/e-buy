import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailedImage extends StatelessWidget {
  final String image;
  const DetailedImage({Key? key, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.7,
      width: MediaQuery.of(context).size.width * 0.7,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 0),
            blurRadius: 10,
            spreadRadius: 3,
            color: Colors.black.withOpacity(0.6),
          )
        ],
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50),
        ),
        image: DecorationImage(image: NetworkImage(image), fit: BoxFit.cover),
      ),
    );
  }
}
