import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InsertImageContainer extends StatelessWidget {
  const InsertImageContainer({
    Key? key,
    required this.isCheck,
    required this.image,
    required this.function,
  }) : super(key: key);

  final bool isCheck;
  final image;
  final dynamic function;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: function,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.2,
          width: MediaQuery.of(context).size.width * 0.4,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                offset: Offset(0, 0),
                blurRadius: 2,
                spreadRadius: 1,
              )
            ],
          ),
          alignment: Alignment.center,
          child: isCheck
              ? Image.file(File(image.path))
              : Text('Upload a image',
                  style: TextStyle(
                    color: Colors.grey,
                  )),
        ),
      ),
    );
  }
}
