import 'package:e_buy/Colors/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtomNavigationBar extends StatelessWidget {
  final dynamic function;
  final String title;
  final Color color;
  ButtomNavigationBar(
      {required this.function, required this.color, required this.title});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: function,
      child: Container(
        alignment: Alignment.center,
        height: MediaQuery.of(context).size.height * 0.1,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(100), topLeft: Radius.circular(100)),
          color: kPrimaryColor,
        ),
        child: Text(
          title,
          style: TextStyle(
              color: color, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
