import 'package:e_buy/Colors/colors.dart';
import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final IconData icon;
  final dynamic function;
  RoundedButton({required this.icon, required this.function});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: GestureDetector(
        onTap: function,
        child: Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, 0),
                  blurRadius: 20,
                  spreadRadius: 0.5,
                  color: Colors.green.withOpacity(0.3),
                )
              ]),
          child: Icon(
            icon,
            color: kPrimaryColor,
            size: 33,
          ),
        ),
      ),
    );
  }
}
