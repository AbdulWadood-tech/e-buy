import 'package:e_buy/Colors/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class DetailedImageContainer extends StatelessWidget {
  final String image;
  final String title;
  final String countryName;
  final String price;
  final bool iSme;
  DetailedImageContainer(
      {required this.image,
      required this.iSme,
      required this.title,
      required this.countryName,
      required this.price});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Neumorphic(
        style: NeumorphicStyle(
            shape: NeumorphicShape.convex,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(12)),
            depth: -2,
            lightSource: LightSource.bottomLeft,
            color: Colors.white),
        child: Container(
          height: 260,
          width: 230,
          child: Column(
            children: [
              Container(
                height: 200,
                width: 230,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(image), fit: BoxFit.cover),
                ),
              ),
              Container(
                height: 60,
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    iSme
                        ? Text(
                            'My Post',
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.bold),
                          )
                        : Text(''),
                    Row(
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                  text: "$title\n".toUpperCase(),
                                  style: Theme.of(context).textTheme.button),
                              TextSpan(
                                text: "$countryName".toUpperCase(),
                                style: TextStyle(
                                  color: kPrimaryColor.withOpacity(0.5),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        Text(
                          '\$$price',
                          style: Theme.of(context)
                              .textTheme
                              .button!
                              .copyWith(color: kPrimaryColor),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
