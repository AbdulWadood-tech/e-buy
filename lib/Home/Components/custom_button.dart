import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../selling_screen.dart';

class CustomFloatingActionButton extends StatelessWidget {
  const CustomFloatingActionButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => SellingScreen()));
      },
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.028),
        height: MediaQuery.of(context).size.height * 0.05,
        width: MediaQuery.of(context).size.width * 0.2,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'SELL',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: MediaQuery.of(context).size.height * 0.015),
            ),
            Icon(
              Icons.add,
              color: Colors.white,
              size: MediaQuery.of(context).size.height * 0.025,
            ),
          ],
        ),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              offset: Offset(0, 0),
              blurRadius: 4,
              spreadRadius: 1,
            )
          ],
          color: Colors.blue,
          borderRadius: BorderRadius.circular(20),
        ),
      ),
    );
  }
}
