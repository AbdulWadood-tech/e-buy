import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class DealingContainer extends StatelessWidget {
  final dynamic function;
  final dynamic functionCall;
  final String city;
  const DealingContainer(
      {Key? key,
      required this.functionCall,
      required this.city,
      required this.function})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 0),
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 1,
                blurRadius: 3),
          ],
        ),
        child: Column(
          children: [
            Text('Listed from $city'),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  FontAwesome.whatsapp,
                  size: 28,
                  color: Colors.green[700],
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Send seller a message',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              width: MediaQuery.of(context).size.width * 0.7,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 0),
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 0.5,
                      blurRadius: 1),
                ],
                borderRadius: BorderRadius.circular(20),
              ),
              child: Center(
                  child: Text(
                'Is this still available',
                style: TextStyle(color: Colors.grey),
              )),
            ),
            SizedBox(
              height: 10,
            ),
            ElevatedButton(
              onPressed: function,
              child: Text('Send'),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 30,
                  child: Divider(
                    thickness: 1,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'OR',
                  style: TextStyle(color: Colors.grey),
                ),
                SizedBox(
                  width: 10,
                ),
                SizedBox(
                  width: 30,
                  child: Divider(
                    thickness: 1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  FontAwesome.phone,
                  size: 28,
                  color: Colors.green[700],
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Call the seller',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            FloatingActionButton(
              elevation: 0.0,
              onPressed: functionCall,
              child: Icon(FontAwesome.phone),
            ),
          ],
        ),
      ),
    );
  }
}
