import 'package:flutter/cupertino.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class CustomIconButton extends StatelessWidget {
  final Icon icon;
  final dynamic function;
  final String title;
  CustomIconButton({required this.icon, required this.title, this.function});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: function,
      child: Neumorphic(
          style: NeumorphicStyle(
              shape: NeumorphicShape.convex,
              boxShape: NeumorphicBoxShape.circle(),
              depth: 5,
              lightSource: LightSource.topLeft,
              color: Colors.white),
          child: Container(
            padding: EdgeInsets.all(10),
            height: 30,
            width: 30,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [icon, Text(title)],
            ),
          )),
    );
  }
}
