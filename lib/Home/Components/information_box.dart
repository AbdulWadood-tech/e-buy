import 'package:flutter/cupertino.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class InformationBox extends StatelessWidget {
  final double? height;
  final dynamic function;
  final String? prefix;
  InformationBox({Key? key, this.height, this.prefix, required this.function})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.05),
      child: Neumorphic(
        style: NeumorphicStyle(
            shape: NeumorphicShape.convex,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(10)),
            depth: -5,
            lightSource: LightSource.topLeft,
            color: Colors.white),
        child: Container(
          padding: EdgeInsets.only(top: 5, left: 5),
          height: (height == null) ? 60 : height,
          width: MediaQuery.of(context).size.width,
          child: TextFormField(
            onChanged: function,
            decoration: InputDecoration(
              prefix: prefix != null
                  ? Text(
                      prefix!,
                      style: TextStyle(color: Colors.black),
                    )
                  : Text(''),
              border: InputBorder.none,
            ),
          ),
        ),
      ),
    );
  }
}
