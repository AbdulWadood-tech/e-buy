import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e_buy/Databas%20Services/database_services.dart';
import 'package:e_buy/Home/Components/detailed_image.dart';
import 'package:e_buy/Models/posts_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'Components/detailed_image_container.dart';
import 'Components/image_container.dart';
import 'buying_deatiled_screen.dart';
import 'buying_screen.dart';
import 'dealing_screen.dart';
import 'my_post.dart';

class MobileListScreen extends StatefulWidget {
  final String id;
  const MobileListScreen({Key? key, required this.id}) : super(key: key);

  @override
  _MobileListScreenState createState() => _MobileListScreenState();
}

class _MobileListScreenState extends State<MobileListScreen> {
  List posts = [];
  @override
  Widget build(BuildContext context) {
    String uid = Provider.of<User?>(context)!.uid;
    return StreamBuilder<QuerySnapshot>(
        stream: DatabaseServices().getUserPost(this.widget.id),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final postsFromFirebase = snapshot.data!.docs;
            for (var post in postsFromFirebase) {
              String isMe = post.get('Uid');
              if (isMe == uid) {
                PostModel model = PostModel(
                    email: post.get('Email'),
                    phoneNo: post.get('Number'),
                    uid: post.get('Uid'),
                    isMe: true,
                    price: post.get('Price'),
                    photoUrl: post.get('PhotoUrl'),
                    description: post.get('Description'),
                    title: post.get('Title'),
                    city: post.get('City'));
                posts.add(model);
              } else {
                PostModel model = PostModel(
                  uid: post.get('Uid'),
                  email: post.get('Email'),
                  phoneNo: post.get('Number'),
                  isMe: false,
                  price: post.get('Price'),
                  photoUrl: post.get('PhotoUrl'),
                  description: post.get('Description'),
                  title: post.get('Title'),
                  city: post.get('City'),
                );
                posts.add(model);
              }
            }
            return Scaffold(
              appBar: AppBar(
                title: Text(this.widget.id),
              ),
              body: Container(
                padding: EdgeInsets.only(top: 20),
                child: Column(
                  children: [
                    Expanded(
                      child: GridView.builder(
                          gridDelegate:
                              SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: 300,
                                  childAspectRatio: 1.47 / 2,
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 10),
                          itemCount: posts.length,
                          itemBuilder: (BuildContext ctx, i) {
                            return GestureDetector(
                              onTap: () {
                                var check = posts[i].isMe;
                                check
                                    ? Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                        return MyPostScreen(
                                            function: () async {
                                              Alert(
                                                context: context,
                                                type: AlertType.error,
                                                title: "Deleting Post",
                                                desc:
                                                    "Are you sure you want to delete",
                                                buttons: [
                                                  DialogButton(
                                                      child: Text(
                                                        "Delete",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 20),
                                                      ),
                                                      onPressed: () {
                                                        DatabaseServices()
                                                            .deleteThePost(
                                                                posts[i]
                                                                    .photoUrl,
                                                                this.widget.id);
                                                        DatabaseServices()
                                                            .deleteTheProfilePost(
                                                                posts[i]
                                                                    .photoUrl,
                                                                uid);
                                                        Navigator.pushAndRemoveUntil(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) {
                                                          return BuyingScreen();
                                                        }), (route) => false);
                                                      })
                                                ],
                                              ).show();
                                            },
                                            docName: this.widget.id,
                                            uid: posts[i].uid,
                                            image: posts[i].photoUrl,
                                            name: posts[i].title,
                                            subName: posts[i].city,
                                            price: posts[i].price);
                                      }))
                                    : Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                        return DetailedScreen(
                                          description: posts[i].description,
                                          uid: posts[i].uid,
                                          email: posts[i].email,
                                          phoneNo: posts[i].phoneNo,
                                          image: posts[i].photoUrl,
                                          name: posts[i].title,
                                          subName: posts[i].city,
                                          price: posts[i].price,
                                          type: this.widget.id,
                                        );
                                      }));
                              },
                              child: Container(
                                child: DetailedImageContainer(
                                    iSme: posts[i].isMe,
                                    image: posts[i].photoUrl,
                                    title: posts[i].title,
                                    countryName: posts[i].city,
                                    price: posts[i].price),
                              ),
                            );
                          }),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Scaffold(
              body: Center(
                child: SpinKitCircle(
                  color: Colors.grey,
                  size: 40,
                ),
              ),
            );
          }
        });
  }
}
