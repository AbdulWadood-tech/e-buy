import 'package:e_buy/Authentication/Services/services.dart';
import 'package:e_buy/Home/profile_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

import 'Components/custom_button.dart';
import 'buying_deatiled_screen.dart';
import 'Components/image_container.dart';

class BuyingScreen extends StatefulWidget {
  const BuyingScreen({Key? key}) : super(key: key);

  @override
  _BuyingScreenState createState() => _BuyingScreenState();
}

class _BuyingScreenState extends State<BuyingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: CustomFloatingActionButton(),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ProfileScreen();
            }));
          },
          icon: Icon(
            Icons.person,
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.logout,
              color: Colors.black,
            ),
            onPressed: () {
              AuthenticationServices().signOutUser();
            },
          ),
        ],
      ),
      body: Center(
        child: Container(
          child: Column(
            children: [
              Text(
                'What You want to Buy',
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey),
              ),
              SizedBox(
                height: 30,
              ),
              Expanded(
                child: GridView.count(
                  primary: false,
                  padding: const EdgeInsets.all(20),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 20,
                  crossAxisCount: 2,
                  children: [
                    GestureDetector(
                      child: ImageContainer(url: 'images/mobile.jpg'),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                BuyingDetailScreen(
                                  id: 'Mobile List',
                                )));
                      },
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/laptop.jpg'),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                BuyingDetailScreen(
                                  id: 'Laptop List',
                                )));
                      },
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/tablets.jpg'),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                BuyingDetailScreen(
                                  id: 'Tablet List',
                                )));
                      },
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/books.jpeg'),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                BuyingDetailScreen(
                                  id: 'Book List',
                                )));
                      },
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/cats.jpg'),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                BuyingDetailScreen(
                                  id: 'Cat List',
                                )));
                      },
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/dogs.jpg'),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                BuyingDetailScreen(
                                  id: 'Dog List',
                                )));
                      },
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/beds.jpg'),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                BuyingDetailScreen(
                                  id: 'Bed List',
                                )));
                      },
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/room.jpg'),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                BuyingDetailScreen(
                                  id: 'Room Equipment List',
                                )));
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
