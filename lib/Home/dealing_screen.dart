import 'dart:collection';

import 'package:e_buy/Colors/colors.dart';
import 'package:e_buy/Home/user_profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Components/buttom_navigation_bar.dart';
import 'Components/detailed_image.dart';

class DetailedScreen extends StatelessWidget {
  final String uid;
  final String image;
  final String name;
  final String subName;
  final String price;
  final String email;
  final String phoneNo;
  final String description;
  final String type;
  DetailedScreen({
    required this.image,
    required this.description,
    required this.uid,
    required this.email,
    required this.phoneNo,
    required this.name,
    required this.subName,
    required this.price,
    required this.type,
  });
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: ButtomNavigationBar(
          color: Colors.white,
          function: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return UserProfile(
                image: image,
                description: description,
                uid: uid,
                email: email,
                phoneNo: phoneNo,
                name: name,
                subName: subName,
                price: price,
                type: type,
              );
            }));
          },
          title: 'Contact the seller',
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.7,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: Icon(
                              Icons.arrow_back,
                              color: kPrimaryColor,
                              size: 40,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  DetailedImage(image: image),
                ],
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: name,
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Colors.grey)),
                        TextSpan(
                          text: '\n$subName',
                          style: TextStyle(fontSize: 20, color: kPrimaryColor),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    '\$$price',
                    style: TextStyle(fontSize: 30, color: kPrimaryColor),
                  )
                ],
              ),
            )
          ],
        ));
  }
}
