import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e_buy/Databas%20Services/database_services.dart';
import 'package:e_buy/Home/Components/featured_photos.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:url_launcher/url_launcher.dart';
import 'Components/dealing_container.dart';

class UserProfile extends StatelessWidget {
  final String image;
  final String description;
  final String name;
  final String subName;
  final String price;
  final String email;
  final String phoneNo;
  final String uid;
  final String type;
  UserProfile({
    required this.image,
    required this.description,
    required this.uid,
    required this.email,
    required this.phoneNo,
    required this.name,
    required this.subName,
    required this.price,
    required this.type,
  });
  @override
  Widget build(BuildContext context) {
    List pictures = [];
    return StreamBuilder<QuerySnapshot>(
        stream: DatabaseServices().getUserPost(type),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final postsFromFirebase = snapshot.data!.docs;
            for (var post in postsFromFirebase) {
              String url = post.get('PhotoUrl');
              if (url == image) {
                print('Are equal');
                pictures = post.get('Multiple photos');
              }
            }
            return Scaffold(
              appBar: AppBar(),
              body: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 260,
                        child: PageView.builder(
                            controller: PageController(viewportFraction: 0.9),
                            itemCount: pictures.length,
                            itemBuilder: (_, i) {
                              return FeaturedFoods(image: pictures[i]);
                            }),
                      ),
                      Text(
                        name,
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        price,
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      DealingContainer(
                        functionCall: () async {
                          var url = "tel:$phoneNo";
                          if (await canLaunch(url)) {
                            await launch(url);
                          } else {
                            throw 'Could not launch $url';
                          }
                        },
                        city: subName,
                        function: () async {
                          print(phoneNo);
                          var whatsappURl = "whatsapp://send?phone=" +
                              phoneNo +
                              "&text=Is this still available. \nI just saw your add on E-BUY.\nLets make a deal.";
                          launch(whatsappURl);
                        },
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: Divider(
                          thickness: 1,
                          height: 40,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Seller Information'),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      StreamBuilder<DocumentSnapshot>(
                          stream: DatabaseServices().getUserData(uid),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              var data = snapshot.data!;
                              String url = data.get('PhotoUrl');
                              String name = data.get('Name');
                              return Row(
                                children: [
                                  CircleAvatar(
                                    radius: 30,
                                    backgroundImage: NetworkImage(url),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    name,
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              );
                            } else {
                              return Center(
                                child: SpinKitCircle(
                                  color: Colors.grey,
                                  size: 20,
                                ),
                              );
                            }
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      Text('Description:',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      SizedBox(
                        height: 10,
                      ),
                      Text(description),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.phone,
                            color: Colors.green[800],
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text("+$phoneNo"),
                        ],
                      ),
                      SizedBox(
                        height: 9,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.email,
                            color: Colors.green[800],
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(email),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                        alignment: Alignment.center,
                        height: MediaQuery.of(context).size.height * 0.1,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(50),
                              topRight: Radius.circular(50)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 0),
                              color: Colors.green.withOpacity(0.1),
                              blurRadius: 5,
                              spreadRadius: 0.5,
                            )
                          ],
                        ),
                        child: Text(
                          'This is all for now',
                          style: TextStyle(
                            color: Colors.black.withOpacity(0.5),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Scaffold(
              body: Center(
                child: SpinKitCircle(
                  size: 40,
                  color: Colors.grey,
                ),
              ),
            );
          }
        });
  }
}
