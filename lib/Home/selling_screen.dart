import 'package:e_buy/Home/Components/iconbutton.dart';
import 'package:e_buy/Home/selling_detailed_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'Components/image_container.dart';

class SellingScreen extends StatefulWidget {
  SellingScreen({Key? key}) : super(key: key);

  @override
  _SellingScreenState createState() => _SellingScreenState();
}

class _SellingScreenState extends State<SellingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
            padding: EdgeInsets.only(top: 20),
            child: Column(
              children: [
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(
                          Entypo.cross,
                          color: Colors.grey,
                          size: 30,
                        )),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      'What You want to Sell',
                      style: TextStyle(
                          fontSize: MediaQuery.of(context).size.height * 0.03,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Expanded(
                  child: GridView.count(
                    primary: false,
                    padding: const EdgeInsets.all(30),
                    crossAxisSpacing: 50,
                    mainAxisSpacing: 50,
                    crossAxisCount: 3,
                    children: [
                      CustomIconButton(
                        icon: Icon(
                          FontAwesome.mobile,
                          size: 30,
                        ),
                        function: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  SellingDetailedScreen(id: 'Mobile List')));
                        },
                        title: 'Mobile',
                      ),
                      CustomIconButton(
                        title: 'Laptop',
                        icon: Icon(
                          FontAwesome.laptop,
                          size: 30,
                        ),
                        function: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  SellingDetailedScreen(id: 'Laptop List')));
                        },
                      ),
                      CustomIconButton(
                        title: 'Tablet',
                        icon: Icon(
                          FontAwesome.tablet,
                          size: 30,
                        ),
                        function: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  SellingDetailedScreen(id: 'Tablet List')));
                        },
                      ),
                      CustomIconButton(
                        title: 'Book',
                        icon: Icon(
                          FontAwesome.book,
                          size: 30,
                        ),
                        function: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  SellingDetailedScreen(id: 'Book List')));
                        },
                      ),
                      CustomIconButton(
                        title: 'Cat',
                        icon: Icon(
                          MaterialCommunityIcons.cat,
                          size: 30,
                        ),
                        function: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  SellingDetailedScreen(id: 'Cat List')));
                        },
                      ),
                      CustomIconButton(
                        title: 'Dog',
                        icon: Icon(
                          SimpleLineIcons.doc,
                          size: 30,
                        ),
                        function: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  SellingDetailedScreen(id: 'Dog List')));
                        },
                      ),
                      CustomIconButton(
                        title: 'Bed',
                        icon: Icon(
                          MaterialCommunityIcons.bed_empty,
                          size: 30,
                        ),
                        function: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  SellingDetailedScreen(id: 'Bed List')));
                        },
                      ),
                      CustomIconButton(
                        title: 'Sofa',
                        icon: Icon(
                          MaterialCommunityIcons.bed_empty,
                          size: 30,
                        ),
                        function: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  SellingDetailedScreen(
                                      id: 'Room Equipment List')));
                        },
                      ),
                      CustomIconButton(
                        title: 'Other',
                        icon: Icon(
                          FontAwesome.book,
                          size: 30,
                        ),
                        function: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  SellingDetailedScreen(id: 'Other List')));
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
