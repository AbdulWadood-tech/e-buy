import 'dart:io';

import 'package:e_buy/Authentication/Services/services.dart';
import 'package:e_buy/Databas%20Services/database_services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

import 'Components/information_box.dart';
import 'Components/insert_image_container.dart';

class SellingDetailedScreen extends StatefulWidget {
  final String id;
  SellingDetailedScreen({Key? key, required this.id}) : super(key: key);
  @override
  _SellingDetailedScreenState createState() => _SellingDetailedScreenState();
}

class _SellingDetailedScreenState extends State<SellingDetailedScreen> {
  final List<String> cities = [
    'Islamabad',
    'Lahore',
    'Karachi',
    'Peshawar',
    'Multan',
    'Kashmir',
    'Hyderabad',
    'Gujranwala'
  ];
  String? price;
  String? title;
  String? description;
  String? city;
  String? photoUrl;
  String? photoUrl2;
  String? photoUrl3;
  String? photoUrl4;
  String? number;
  String? email;
  // Map<String, String> picsMap = {};
  List pictures = [];
  var image;
  var image1;
  var image2;
  var image3;
  bool isCheck = false;
  bool isCheck1 = false;
  bool isCheck2 = false;
  bool isCheck3 = false;
  bool spinner = false;
  File? url;
  File? url1;
  File? url2;
  File? url3;
  spinnerOn() {
    setState(() {
      spinner = !spinner;
    });
  }

  @override
  Widget build(BuildContext context) {
    String uid = Provider.of<User?>(context)!.uid;
    email = AuthenticationServices().getEmail();
    return Scaffold(
      appBar: AppBar(
        title: Text('Include some details'),
        backgroundColor: Colors.blue[400],
      ),
      body: spinner
          ? Center(
              child: SpinKitCircle(
                size: 40,
                color: Colors.grey,
              ),
            )
          : Container(
              padding: EdgeInsets.symmetric(horizontal: 14, vertical: 20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            InsertImageContainer(
                              isCheck: isCheck,
                              image: image,
                              function: () async {
                                image = await DatabaseServices().selectFile();
                                setState(() {
                                  if (image != null) {
                                    isCheck = true;
                                  }
                                });
                              },
                            ),
                            InsertImageContainer(
                              isCheck: isCheck1,
                              image: image1,
                              function: () async {
                                image1 = await DatabaseServices().selectFile();
                                setState(() {
                                  if (image1 != null) {
                                    isCheck1 = true;
                                  }
                                });
                              },
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            InsertImageContainer(
                              isCheck: isCheck2,
                              image: image2,
                              function: () async {
                                image2 = await DatabaseServices().selectFile();
                                setState(() {
                                  if (image2 != null) {
                                    isCheck2 = true;
                                  }
                                });
                              },
                            ),
                            InsertImageContainer(
                              isCheck: isCheck3,
                              image: image3,
                              function: () async {
                                image3 = await DatabaseServices().selectFile();
                                setState(() {
                                  if (image3 != null) {
                                    isCheck3 = true;
                                  }
                                });
                              },
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          'Price',
                          style: TextStyle(color: Colors.grey),
                        ),
                        InformationBox(
                          function: (val) {
                            price = val;
                            print('$price');
                          },
                        ),
                        Text(
                          'ad Title',
                          style: TextStyle(color: Colors.grey),
                        ),
                        InformationBox(
                          function: (val) {
                            title = val;
                          },
                        ),
                        Text(
                          'Phone number',
                          style: TextStyle(color: Colors.grey),
                        ),
                        InformationBox(
                          prefix: '+',
                          function: (val) {
                            number = val;
                          },
                        ),
                        Text(
                          'Describe what are you selling',
                          style: TextStyle(color: Colors.grey),
                        ),
                        InformationBox(
                          height: MediaQuery.of(context).size.height * 0.2,
                          function: (val) {
                            description = val;
                          },
                        ),
                        Text(
                          'Select your city',
                          style: TextStyle(color: Colors.grey),
                        ),
                        DropdownButtonFormField(
                          items: cities.map((e) {
                            return DropdownMenuItem(value: e, child: Text(e));
                          }).toList(),
                          onChanged: (val) {
                            city = val as String?;
                          },
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      style: ButtonStyle(alignment: Alignment.center),
                      onPressed: () async {
                        try {
                          spinnerOn();
                          url = File(image.path);
                          url1 = File(image1.path);
                          url2 = File(image2.path);
                          url3 = File(image3.path);
                          photoUrl =
                              await DatabaseServices().addPhotoTStorage(url!);
                          photoUrl2 =
                              await DatabaseServices().addPhotoTStorage(url1!);
                          photoUrl3 =
                              await DatabaseServices().addPhotoTStorage(url2!);
                          photoUrl4 =
                              await DatabaseServices().addPhotoTStorage(url3!);
                          pictures.add(photoUrl!);
                          pictures.add(photoUrl2!);
                          pictures.add(photoUrl3!);
                          pictures.add(photoUrl4!);
                          // picsMap['1'] = photoUrl!;
                          // picsMap['2'] = photoUrl2!;
                          // picsMap['3'] = photoUrl3!;
                          // picsMap['4'] = photoUrl4!;

                          DatabaseServices().addPostWithMultiple(
                              this.widget.id,
                              uid,
                              title!,
                              price!,
                              description!,
                              photoUrl!,
                              city!,
                              email!,
                              number!,
                              pictures);
                          DatabaseServices().addPostToProfile(
                              uid,
                              title!,
                              price!,
                              description!,
                              photoUrl!,
                              city!,
                              email!,
                              number!);
                          Navigator.pop(context);
                        } catch (e) {
                          spinnerOn();
                        }
                      },
                      child: Text('Post'),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}

// GestureDetector(
// onTap: () async {
// image = await DatabaseServices().selectFile();
// setState(() {
// if (image != null) {
// isCheck = true;
// }
// });
// },
// child: NeumorphicIcon(
// Icons.add_circle,
// size: 70,
// style: NeumorphicStyle(
// shape: NeumorphicShape.concave,
// boxShape: NeumorphicBoxShape.roundRect(
// BorderRadius.circular(40)),
// depth: 5,
// lightSource: LightSource.topLeft,
// color: Colors.white),
// ),
// ),
