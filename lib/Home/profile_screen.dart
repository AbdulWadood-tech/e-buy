import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e_buy/Databas%20Services/database_services.dart';
import 'package:e_buy/Home/Components/detailed_image_container.dart';
import 'package:e_buy/Models/posts_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    String uid = Provider.of<User?>(context)!.uid;
    return StreamBuilder<DocumentSnapshot>(
        stream:
            DatabaseServices().getUserData(Provider.of<User?>(context)!.uid),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              backgroundColor: Colors.blue[200],
              body: Stack(
                children: [
                  Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.9,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(40),
                                topRight: Radius.circular(40))),
                      )),
                  Positioned(
                    top: MediaQuery.of(context).size.height * 0.06,
                    left: MediaQuery.of(context).size.height * 0.166,
                    right: MediaQuery.of(context).size.height * 0.166,
                    child: Container(
                      height: 115,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 0),
                            color: Colors.black.withOpacity(0.5),
                            blurRadius: 10,
                            spreadRadius: 1,
                          )
                        ],
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                            image: NetworkImage(snapshot.data!
                                    .get('PhotoUrl') ??
                                'https://miro.medium.com/max/700/0*H3jZONKqRuAAeHnG.jpg'),
                            fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  Positioned(
                      top: MediaQuery.of(context).size.height * 0.141,
                      left: MediaQuery.of(context).size.height * 0.26,
                      right: MediaQuery.of(context).size.height * 0.15,
                      child: IconButton(
                        icon: Icon(Icons.add),
                        iconSize: 50,
                        color: Colors.white,
                        onPressed: () async {
                          var url = await DatabaseServices().selectFile();
                          DatabaseServices().updateProfile(uid, url);
                        },
                      )),
                  Positioned(
                      left: 0,
                      right: 0,
                      bottom: 0,
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 30),
                                height:
                                    MediaQuery.of(context).size.height * 0.2,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.2),
                                      offset: Offset(0, 0),
                                      spreadRadius: 1,
                                      blurRadius: 10,
                                    )
                                  ],
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Name:',
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.grey),
                                        ),
                                        Text(snapshot.data!.get('Name'),
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.grey)),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Email:',
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.grey),
                                        ),
                                        Text(snapshot.data!.get('Email'),
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.grey)),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Phone:',
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.grey),
                                        ),
                                        Text('03055260095',
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.grey)),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Text(
                                'My Posts',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            MyPosts(),
                          ],
                        ),
                      ))
                ],
              ),
            );
          } else {
            return Scaffold(
              body: Center(
                child: SpinKitCircle(
                  color: Colors.grey,
                  size: 40,
                ),
              ),
            );
          }
        });
  }
}

class MyPosts extends StatefulWidget {
  @override
  _MyPostsState createState() => _MyPostsState();
}

class _MyPostsState extends State<MyPosts> {
  List posts = [];

  @override
  Widget build(BuildContext context) {
    String uid = Provider.of<User?>(context)!.uid;
    return StreamBuilder<QuerySnapshot>(
        stream: DatabaseServices().getProfilePost(uid),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final postsFromFirebase = snapshot.data!.docs;
            for (var post in postsFromFirebase) {
              PostModel model = PostModel(
                  uid: '',
                  email: post.get('Email'),
                  phoneNo: post.get('Number'),
                  isMe: false,
                  price: post.get('Price'),
                  photoUrl: post.get('PhotoUrl'),
                  description: post.get('Description'),
                  title: post.get('Title'),
                  city: post.get('City'));
              posts.add(model);
            }
            return Container(
              height: 260,
              child: PageView.builder(
                  controller: PageController(viewportFraction: 0.6),
                  itemCount: posts.length,
                  itemBuilder: (_, i) {
                    return GestureDetector(
                      onLongPress: () {
                        print('hy');
                      },
                      child: DetailedImageContainer(
                          iSme: false,
                          image: posts[i].photoUrl,
                          title: posts[i].title,
                          countryName: posts[i].city,
                          price: posts[i].price),
                    );
                  }),
            );
          } else {
            return SpinKitCircle(
              color: Colors.grey,
              size: 40,
            );
          }
        });
  }
}
