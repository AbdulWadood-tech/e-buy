import 'package:e_buy/Colors/colors.dart';
import 'package:e_buy/Databas%20Services/database_services.dart';
import 'package:e_buy/Home/buying_deatiled_screen.dart';
import 'package:e_buy/Home/buying_screen.dart';
import 'package:e_buy/Home/user_profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Components/buttom_navigation_bar.dart';
import 'Components/detailed_image.dart';

class MyPostScreen extends StatelessWidget {
  final String uid;
  final String image;
  final String name;
  final String subName;
  final String price;
  final String docName;
  final dynamic function;
  MyPostScreen(
      {required this.image,
      required this.docName,
      required this.function,
      required this.uid,
      required this.name,
      required this.subName,
      required this.price});
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: ButtomNavigationBar(
          color: Colors.white,
          title: 'Delete the post',
          function: function,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.7,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: Icon(
                              Icons.arrow_back,
                              color: kPrimaryColor,
                              size: 40,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  DetailedImage(image: image),
                ],
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: name,
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Colors.grey)),
                        TextSpan(
                          text: '\n$subName',
                          style: TextStyle(fontSize: 20, color: kPrimaryColor),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    '\$$price',
                    style: TextStyle(fontSize: 30, color: kPrimaryColor),
                  )
                ],
              ),
            )
          ],
        ));
  }
}
