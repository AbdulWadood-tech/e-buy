import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e_buy/Databas%20Services/database_services.dart';
import 'package:e_buy/Home/Components/detailed_image_container.dart';
import 'package:e_buy/Home/Components/featured_photos.dart';
import 'package:e_buy/Home/dealing_screen.dart';
import 'package:e_buy/Home/mobile_list_screen.dart';
import 'package:e_buy/Home/my_post.dart';
import 'package:e_buy/Models/posts_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'buying_screen.dart';

class BuyingDetailScreen extends StatefulWidget {
  final String id;
  BuyingDetailScreen({Key? key, required this.id}) : super(key: key);

  @override
  _BuyingDetailScreenState createState() => _BuyingDetailScreenState();
}

class _BuyingDetailScreenState extends State<BuyingDetailScreen> {
  List posts = [];
  bool search = false;
  searchToggle() {
    setState(() {
      search = !search;
    });
  }

  var queryResultSet = [];
  var tempResultStore = [];

  initiateSearch(value) {
    if (value.length == 0) {
      setState(() {
        queryResultSet = [];
        tempResultStore = [];
      });
    }
    var capitalizedValue =
        value.substring(0, 1).toUpperCase() + value.substring(1);
    if (queryResultSet.length == 0 && value.length == 1) {
      DatabaseServices()
          .searchByName(value, this.widget.id)
          .then((QuerySnapshot snapshot) {
        for (int i = 0; i < snapshot.docs.length; ++i) {
          queryResultSet.add(snapshot.docs[i].data());
        }
      });
    } else {
      tempResultStore = [];
      queryResultSet.forEach((element) {
        if (element['Title'].startsWith(capitalizedValue)) {
          setState(() {
            tempResultStore.add(element);
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String uid = Provider.of<User?>(context)!.uid;
    return StreamBuilder<QuerySnapshot>(
        stream: DatabaseServices().getUserPost(this.widget.id),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final postsFromFirebase = snapshot.data!.docs;
            for (var post in postsFromFirebase) {
              String isMe = post.get('Uid');
              if (isMe == uid) {
                PostModel model = PostModel(
                    email: post.get('Email'),
                    phoneNo: post.get('Number'),
                    uid: post.get('Uid'),
                    isMe: true,
                    price: post.get('Price'),
                    photoUrl: post.get('PhotoUrl'),
                    description: post.get('Description'),
                    title: post.get('Title'),
                    city: post.get('City'));
                posts.add(model);
              } else {
                PostModel model = PostModel(
                  uid: post.get('Uid'),
                  email: post.get('Email'),
                  phoneNo: post.get('Number'),
                  isMe: false,
                  price: post.get('Price'),
                  photoUrl: post.get('PhotoUrl'),
                  description: post.get('Description'),
                  title: post.get('Title'),
                  city: post.get('City'),
                );
                posts.add(model);
              }
            }
            return Scaffold(
              appBar: AppBar(
                title: Text(
                  this.widget.id,
                  style: TextStyle(color: Colors.grey),
                ),
                leading: IconButton(
                  color: Colors.grey,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back_outlined),
                ),
                backgroundColor: Colors.transparent,
                elevation: 0.0,
              ),
              body: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Neumorphic(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    style: NeumorphicStyle(
                        shape: NeumorphicShape.convex,
                        boxShape: NeumorphicBoxShape.roundRect(
                            BorderRadius.circular(20)),
                        depth: -5,
                        lightSource: LightSource.topLeft,
                        color: Colors.white),
                    child: Container(
                      padding: EdgeInsets.only(top: 5, left: 5),
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      child: TextFormField(
                        onChanged: (val) {
                          if (search != true) {
                            searchToggle();
                          }
                          initiateSearch(val);
                        },
                        decoration: InputDecoration(
                          hintText: 'Search',
                          hintStyle: TextStyle(fontSize: 14),
                          border: InputBorder.none,
                          suffixIcon: Icon(
                            Icons.search,
                            color: Colors.black38,
                          ),
                        ),
                      ),
                    ),
                  ),
                  search
                      ? Expanded(
                          child: GridView.count(
                            crossAxisCount: 2,
                            crossAxisSpacing: 3.0,
                            mainAxisSpacing: 3.0,
                            childAspectRatio: 1.47 / 2,
                            primary: false,
                            shrinkWrap: true,
                            children: tempResultStore.map((e) {
                              return GestureDetector(
                                onTap: () {
                                  if (uid == e['Uid']) {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return MyPostScreen(
                                          image: e['PhotoUrl'],
                                          docName: this.widget.id,
                                          function: () async {
                                            Alert(
                                              context: context,
                                              type: AlertType.error,
                                              title: "Deleting Post",
                                              desc:
                                                  "Are you sure you want to delete",
                                              buttons: [
                                                DialogButton(
                                                    child: Text(
                                                      "Delete",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 20),
                                                    ),
                                                    onPressed: () {
                                                      DatabaseServices()
                                                          .deleteThePost(
                                                              e['PhotoUrl'],
                                                              this.widget.id);
                                                      DatabaseServices()
                                                          .deleteTheProfilePost(
                                                              e['PhotoUrl'],
                                                              uid);
                                                      Navigator
                                                          .pushAndRemoveUntil(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (context) {
                                                        return BuyingScreen();
                                                      }), (route) => false);
                                                    })
                                              ],
                                            ).show();
                                          },
                                          uid: uid,
                                          name: e['Title'],
                                          subName: e['City'],
                                          price: e['Price']);
                                    }));
                                  } else {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return DetailedScreen(
                                        description: e['Description'],
                                        uid: e['Uid'],
                                        email: e['Email'],
                                        phoneNo: e['Number'],
                                        image: e['PhotoUrl'],
                                        name: e['Title'],
                                        subName: e['City'],
                                        price: e['Price'],
                                        type: this.widget.id,
                                      );
                                    }));
                                  }
                                },
                                child: buildResultCard(e, uid),
                              );
                            }).toList(),
                          ),
                        )
                      : Expanded(
                          child: Container(
                            child: SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    children: [
                                      SizedBox(
                                        height: 30,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              '${this.widget.id}',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            ElevatedButton(
                                              onPressed: () {
                                                Navigator.push(context,
                                                    MaterialPageRoute(
                                                        builder: (context) {
                                                  return MobileListScreen(
                                                      id: this.widget.id);
                                                }));
                                              },
                                              child: Text('more'),
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty.all(
                                                        Colors.green[600]),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 30,
                                      ),
                                      Container(
                                        height: 260,
                                        child: PageView.builder(
                                            controller: PageController(
                                                viewportFraction: 0.6),
                                            itemCount: posts.length,
                                            itemBuilder: (_, i) {
                                              return GestureDetector(
                                                onTap: () {
                                                  var check = posts[i].isMe;
                                                  check
                                                      ? Navigator.push(context,
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) {
                                                          return MyPostScreen(
                                                              function:
                                                                  () async {
                                                                Alert(
                                                                  context:
                                                                      context,
                                                                  type: AlertType
                                                                      .error,
                                                                  title:
                                                                      "Deleting Post",
                                                                  desc:
                                                                      "Are you sure you want to delete",
                                                                  buttons: [
                                                                    DialogButton(
                                                                        child:
                                                                            Text(
                                                                          "Delete",
                                                                          style: TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 20),
                                                                        ),
                                                                        onPressed:
                                                                            () {
                                                                          DatabaseServices().deleteThePost(
                                                                              posts[i].photoUrl,
                                                                              this.widget.id);
                                                                          DatabaseServices().deleteTheProfilePost(
                                                                              posts[i].photoUrl,
                                                                              uid);
                                                                          Navigator.pushAndRemoveUntil(
                                                                              context,
                                                                              MaterialPageRoute(builder: (context) {
                                                                            return BuyingScreen();
                                                                          }), (route) => false);
                                                                        })
                                                                  ],
                                                                ).show();
                                                              },
                                                              docName: this
                                                                  .widget
                                                                  .id,
                                                              uid: posts[i].uid,
                                                              image: posts[i]
                                                                  .photoUrl,
                                                              name: posts[i]
                                                                  .title,
                                                              subName:
                                                                  posts[i].city,
                                                              price: posts[i]
                                                                  .price);
                                                        }))
                                                      : Navigator.push(context,
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) {
                                                          return DetailedScreen(
                                                            description: posts[
                                                                    i]
                                                                .description,
                                                            uid: posts[i].uid,
                                                            email:
                                                                posts[i].email,
                                                            phoneNo: posts[i]
                                                                .phoneNo,
                                                            image: posts[i]
                                                                .photoUrl,
                                                            name:
                                                                posts[i].title,
                                                            subName:
                                                                posts[i].city,
                                                            price:
                                                                posts[i].price,
                                                            type:
                                                                this.widget.id,
                                                          );
                                                        }));
                                                },
                                                child: Container(
                                                  child: DetailedImageContainer(
                                                      iSme: posts[i].isMe,
                                                      image: posts[i].photoUrl,
                                                      title: posts[i].title,
                                                      countryName:
                                                          posts[i].city,
                                                      price: posts[i].price),
                                                ),
                                              );
                                            }),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 20),
                                        child: Text(
                                          'Photos',
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      Container(
                                        height: 260,
                                        child: PageView.builder(
                                            controller: PageController(
                                                viewportFraction: 0.9),
                                            itemCount: posts.length,
                                            itemBuilder: (_, i) {
                                              return FeaturedFoods(
                                                  image: posts[i].photoUrl);
                                            }),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                ],
              ),
            );
          } else {
            return Scaffold(
              body: Center(
                child: SpinKitCircle(
                  color: Colors.grey,
                  size: 50,
                ),
              ),
            );
          }
        });
  }
}

Widget buildResultCard(e, String uid) {
  String image = e['PhotoUrl'];
  String title = e['Title'];
  String countryName = e['City'];
  String price = e['Price'];
  String uidFromDb = e['Uid'];
  if (uid == uidFromDb) {
    return DetailedImageContainer(
        image: image,
        iSme: true,
        title: title,
        countryName: countryName,
        price: price);
  } else {
    return DetailedImageContainer(
        image: image,
        iSme: false,
        title: title,
        countryName: countryName,
        price: price);
  }
}
