import 'package:flutter/material.dart';

var buttonDecoration = BoxDecoration(
  color: Colors.white,
  shape: BoxShape.circle,
  boxShadow: [
    BoxShadow(
        color: Colors.black38,
        offset: Offset(4.0, 4.0),
        blurRadius: 15.0,
        spreadRadius: 1.0),
    BoxShadow(
        color: Colors.white,
        offset: Offset(-4.0, -4.0),
        blurRadius: 15.0,
        spreadRadius: 1.0),
  ],
);

// gradient: LinearGradient(
// begin: Alignment.topLeft,
// end: Alignment.bottomRight,
// colors: [
// Colors.white10,
// Colors.white12,
// Colors.white30,
// Colors.white10,
// ])
