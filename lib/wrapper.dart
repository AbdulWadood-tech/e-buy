import 'package:e_buy/Authentication/Services/toggle.dart';
import 'package:e_buy/Home/buying_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class WrapperClass extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider.of<User?>(context) != null ? BuyingScreen() : Toggle();
  }
}
