import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';

class DatabaseServices {
  //reference for users
  final CollectionReference reference =
      FirebaseFirestore.instance.collection('Users');

  //reference for posts
  final CollectionReference postsReference =
      FirebaseFirestore.instance.collection('Posts');

  //reference for profile
  final CollectionReference profileReference =
      FirebaseFirestore.instance.collection('Profile');

  //add post to their categories
  Future addPost(
      String type,
      String uid,
      String title,
      String price,
      String description,
      String photoUrl,
      String city,
      String email,
      String number) async {
    return await postsReference.doc(type).collection('Users').doc().set({
      'Title': '${title[0].toUpperCase()}${title.substring(1)}',
      'Price': price,
      'Description': description,
      'PhotoUrl': photoUrl,
      'City': city,
      'Uid': uid,
      'Email': email,
      'Number': number,
      'Key': title[0].toUpperCase(),
    });
  }

  //add post to profile
  Future addPostToProfile(
      String uid,
      String title,
      String price,
      String description,
      String photoUrl,
      String city,
      String email,
      String number) async {
    return await profileReference.doc(uid).collection('Posts').doc().set({
      'Title': title,
      'Price': price,
      'Description': description,
      'PhotoUrl': photoUrl,
      'City': city,
      'Email': email,
      'Number': number,
    });
  }

  //add users (when they signup first time) to database
  Future addUserToDatabase(
      String uid, String name, String photoUrl, String email) async {
    return await reference.doc(uid).set({
      'Uid': uid,
      'Name': name,
      'PhotoUrl': photoUrl,
      'Email': email,
    });
  }

  Future updateProfile(String uid, String url) async {
    return await reference.doc(uid).update({'PhotoUrl': url});
  }

  //pick file from gallery and upload it to firebase storage
  Future selectFile() async {
    final _picker = ImagePicker();
    var image;
    image = await _picker.pickImage(source: ImageSource.gallery);
    return image;
  }

  //add photo to storage
  Future addPhotoTStorage(File file) async {
    final storage = FirebaseStorage.instance;
    var snapshot = await storage.ref().child(file.path).putFile(file);
    String url = await snapshot.ref.getDownloadURL();
    print(url);
    return url;
  }

  //get User data from fire store
  Stream<DocumentSnapshot> getUserData(String uid) {
    return reference.doc(uid).snapshots();
  }

  //get posts stream
  Stream<QuerySnapshot> getUserPost(String id) {
    return postsReference.doc(id).collection('Users').snapshots();
  }

  //get posts in profile stream
  Stream<QuerySnapshot> getProfilePost(String uid) {
    return profileReference.doc(uid).collection('Posts').snapshots();
  }

  //delete post from fire store
  Future deleteThePost(String photoUrl, String docName) async {
    postsReference
        .doc(docName)
        .collection('Users')
        .where('PhotoUrl', isEqualTo: photoUrl)
        .get()
        .then((value) {
      value.docs.forEach((element) {
        FirebaseFirestore.instance
            .collection('Posts')
            .doc(docName)
            .collection('Users')
            .doc(element.id)
            .delete()
            .then((value) {
          print('Success!');
        });
      });
    });
  }

  //delete post from profile
  Future deleteTheProfilePost(String photoUrl, String uid) async {
    await profileReference
        .doc(uid)
        .collection('Posts')
        .where('PhotoUrl', isEqualTo: photoUrl)
        .get()
        .then((value) {
      value.docs.forEach((element) {
        FirebaseFirestore.instance
            .collection('Profile')
            .doc(uid)
            .collection('Posts')
            .doc(element.id)
            .delete()
            .then((value) {
          print('Success!');
        });
      });
    });
  }

  //Search in fire store for document
  searchByName(String searchField, String id) {
    return postsReference
        .doc(id)
        .collection('Users')
        .where('Key', isEqualTo: searchField.substring(0, 1).toUpperCase())
        .get();
  }

  //add multiple photos to database using maps
  Future addPostWithMultiple(
    String type,
    String uid,
    String title,
    String price,
    String description,
    String photoUrl,
    String city,
    String email,
    String number,
    List pictures,
  ) async {
    return await postsReference.doc(type).collection('Users').doc().set({
      'Title': '${title[0].toUpperCase()}${title.substring(1)}',
      'Price': price,
      'Description': description,
      'PhotoUrl': photoUrl,
      'City': city,
      'Uid': uid,
      'Email': email,
      'Number': number,
      'Key': title[0].toUpperCase(),
      'Multiple photos': pictures
    });
  }

  //get Multiple photos

}
